use itertools::Itertools;

const INPUT: &str = include_str!("input.txt");
fn main() {
    println!("Hello, world!");

    let total: i32 = INPUT
        .split_whitespace()
        .map(|line| line.parse().unwrap())
        .tuple_windows()
        .map(|(prev,_,_,curr): (i32,_,_,_)| if curr > prev { 1 } else { 0 })
        .sum();
    println!("total = {}", total);
}
