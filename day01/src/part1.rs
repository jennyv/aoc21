use itertools::Itertools;
const INPUT: &str = include_str!("input.txt");
fn main() {
    println!("Hello, world!");
    let total = INPUT
        .split_whitespace()
        .map(|line| line.parse().unwrap())
        .tuple_windows::<(i32, i32)>()
        .map(|(prev, curr)| if curr > prev { 1 } else { 0 })
        .sum::<i32>();
    println!("total = {}", total);
}
