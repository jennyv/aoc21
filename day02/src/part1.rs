use nom::{ 
    branch::alt,
    bytes::complete::tag,
    character::complete::{char, i32, line_ending},
    combinator::{map, value},
    multi::separated_list1,
    sequence::separated_pair,
    IResult,
};

#[derive(Debug)]
struct ParsedInput {
    instructions: Vec<Instruction>,
}

#[derive(Debug)]
struct Instruction {
    kind: InstructionKind,
    magnitude: i32,
}

#[derive(Copy, Clone, Debug,PartialEq)]
enum InstructionKind {
    Forward,
    Up,
    Down,
}

fn parse(input: &str) -> IResult<&str, ParsedInput> {
    let forward = value(InstructionKind::Forward, tag("forward"));
    let up = value(InstructionKind::Up, tag("up"));
    let down = value(InstructionKind::Down, tag("down"));
    let instruction_kind = alt((forward, up, down));
    let instruction = map(
        separated_pair(instruction_kind, char(' '), i32),
        |(kind, magnitude)| Instruction { kind, magnitude },
    );
    let instructions = separated_list1(line_ending, instruction);
    let mut parse = map(instructions, |instructions| ParsedInput { instructions });
    parse(input)
}


const INPUT: &str = include_str!("input.txt");
fn main() {
    println!("Hello, world!");
    let mut depth = 0;
    let mut forward = 0;
    if let Ok((_, result)) = parse(INPUT)
    {
        for i in 0..result.instructions.len()
        {
            if result.instructions[i].kind == InstructionKind::Forward
            {
                forward += result.instructions[i].magnitude;
            }
            if result.instructions[i].kind == InstructionKind::Up
            {
                depth -= result.instructions[i].magnitude;
            }
            if result.instructions[i].kind == InstructionKind::Down
            {
                depth += result.instructions[i].magnitude;
            }
        }
    }
    println!("depth : {}, forward : {}, product : {}",depth,forward, forward*depth);
}
